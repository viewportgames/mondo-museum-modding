﻿using UnityEngine;

namespace Viewport.MondoMuseum {

	[CreateAssetMenu(fileName = "New Display Case Data", menuName = "Mondo Museum Assets/Display Case Base", order = 51)]
	[System.Serializable]
	public class DisplayCaseScriptableObject : ScriptableObject {
		[SerializeField] private GameObject[] _highDisplayCasePrefabs;
		public GameObject[] HighDisplayCasePrefabs{
			get { return _highDisplayCasePrefabs; }
			set { _highDisplayCasePrefabs = value; }
		}
		[SerializeField] private GameObject[] _mediumDisplayCasePrefabs;
		public GameObject[] MediumDisplayCasePrefabs{
			get { return _mediumDisplayCasePrefabs; }
			set { _mediumDisplayCasePrefabs = value; }
		} 
		[SerializeField] private GameObject[] _lowDisplayCasePrefabs;
		public GameObject[] LowDisplayCasePrefabs{
			get { return _lowDisplayCasePrefabs; }
			set { _lowDisplayCasePrefabs = value; }
		}
	}
}