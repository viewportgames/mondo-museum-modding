﻿using UnityEngine;

namespace Viewport.MondoMuseum {

	[CreateAssetMenu(fileName = "New Exhibit Hall Icon Data", menuName = "Mondo Museum Assets/Exhibit Hall Icons", order = 70)]
	[System.Serializable]
	public class IconScriptableObject : ScriptableObject {
		[SerializeField] private Texture2D[] _exhibitIconTextures = null;
		public Texture2D[] ExhibitIconTextures{
			get { return _exhibitIconTextures; }
		}

	}
}