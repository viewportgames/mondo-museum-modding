﻿using UnityEngine;
using UnityEditor;

namespace Viewport.MondoMuseum {

	[CustomEditor(typeof(DisplayCaseGlassScriptableObject), true)]
	[CanEditMultipleObjects]
	public class DisplayCaseGlassScriptableObjectEditor : Editor{
	
		DisplayCaseGlassScriptableObject _script;

		private bool _foldoutGlassList = true;

		private void OnEnable(){
			_script = target as DisplayCaseGlassScriptableObject;

			if(_script.DisplayCaseGlassPrefabs == null || _script.DisplayCaseGlassPrefabs.Length < 10){
				_script.DisplayCaseGlassPrefabs = new GameObject[10];
			}
		}

		public override void OnInspectorGUI(){
			serializedObject.Update();

			_foldoutGlassList = EditorGUILayout.Foldout(_foldoutGlassList, "Display Case Glass Prefabs");
			if(_foldoutGlassList){
				EditorGUI.indentLevel++;
				_script.DisplayCaseGlassPrefabs[0] = (GameObject)EditorGUILayout.ObjectField("1x1", _script.DisplayCaseGlassPrefabs[0], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[1] = (GameObject)EditorGUILayout.ObjectField("1x2", _script.DisplayCaseGlassPrefabs[1], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[2] = (GameObject)EditorGUILayout.ObjectField("1x3", _script.DisplayCaseGlassPrefabs[2], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[3] = (GameObject)EditorGUILayout.ObjectField("1x4", _script.DisplayCaseGlassPrefabs[3], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[4] = (GameObject)EditorGUILayout.ObjectField("2x2", _script.DisplayCaseGlassPrefabs[4], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[5] = (GameObject)EditorGUILayout.ObjectField("2x3", _script.DisplayCaseGlassPrefabs[5], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[6] = (GameObject)EditorGUILayout.ObjectField("2x4", _script.DisplayCaseGlassPrefabs[6], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[7] = (GameObject)EditorGUILayout.ObjectField("3x3", _script.DisplayCaseGlassPrefabs[7], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[8] = (GameObject)EditorGUILayout.ObjectField("3x4", _script.DisplayCaseGlassPrefabs[8], typeof(GameObject), false);
				_script.DisplayCaseGlassPrefabs[9] = (GameObject)EditorGUILayout.ObjectField("4x4", _script.DisplayCaseGlassPrefabs[9], typeof(GameObject), false);
				EditorGUI.indentLevel--;
			}

			foreach(GameObject obj in _script.DisplayCaseGlassPrefabs){
				if(obj == null){
					EditorGUILayout.HelpBox("Missing Glass Prefabs!", MessageType.Error);
					break;
				}
			}


			serializedObject.ApplyModifiedProperties();

			if(GUI.changed){
				EditorUtility.SetDirty(_script);
			}
		}
	}
}
