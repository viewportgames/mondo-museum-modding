﻿using UnityEngine;
using UnityEditor;

namespace Viewport.MondoMuseum {

	[CustomEditor(typeof(DisplayCaseScriptableObject), true)]
	[CanEditMultipleObjects]
	public class DisplayCaseScriptableObjectEditor : Editor{
		DisplayCaseScriptableObject _script;

		private bool _foldoutHighDisplayCaseList = false;
		private bool _foldoutMediumDisplayCaseList = false;
		private bool _foldoutLowDisplayCaseList = false;

		private void OnEnable(){
			_script = target as DisplayCaseScriptableObject;

			if(_script.HighDisplayCasePrefabs == null || _script.HighDisplayCasePrefabs.Length < 10){
				_script.HighDisplayCasePrefabs = new GameObject[10];
			}
			if(_script.MediumDisplayCasePrefabs == null || _script.MediumDisplayCasePrefabs.Length < 10){
				_script.MediumDisplayCasePrefabs = new GameObject[10];
			}
			if(_script.LowDisplayCasePrefabs == null || _script.LowDisplayCasePrefabs.Length < 10){
				_script.LowDisplayCasePrefabs = new GameObject[10];
			}
		}

		public override void OnInspectorGUI(){
			serializedObject.Update();

			_foldoutHighDisplayCaseList = EditorGUILayout.Foldout(_foldoutHighDisplayCaseList, "High Prefabs");
			if(_foldoutHighDisplayCaseList){
				EditorGUI.indentLevel++;
				_script.HighDisplayCasePrefabs[0] = (GameObject)EditorGUILayout.ObjectField("1x1", _script.HighDisplayCasePrefabs[0], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[1] = (GameObject)EditorGUILayout.ObjectField("1x2", _script.HighDisplayCasePrefabs[1], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[2] = (GameObject)EditorGUILayout.ObjectField("1x3", _script.HighDisplayCasePrefabs[2], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[3] = (GameObject)EditorGUILayout.ObjectField("1x4", _script.HighDisplayCasePrefabs[3], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[4] = (GameObject)EditorGUILayout.ObjectField("2x2", _script.HighDisplayCasePrefabs[4], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[5] = (GameObject)EditorGUILayout.ObjectField("2x3", _script.HighDisplayCasePrefabs[5], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[6] = (GameObject)EditorGUILayout.ObjectField("2x4", _script.HighDisplayCasePrefabs[6], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[7] = (GameObject)EditorGUILayout.ObjectField("3x3", _script.HighDisplayCasePrefabs[7], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[8] = (GameObject)EditorGUILayout.ObjectField("3x4", _script.HighDisplayCasePrefabs[8], typeof(GameObject), false);
				_script.HighDisplayCasePrefabs[9] = (GameObject)EditorGUILayout.ObjectField("4x4", _script.HighDisplayCasePrefabs[9], typeof(GameObject), false);
				EditorGUI.indentLevel--;
			}
			_foldoutMediumDisplayCaseList = EditorGUILayout.Foldout(_foldoutMediumDisplayCaseList, "Medium Prefabs");
			if(_foldoutMediumDisplayCaseList){
				EditorGUI.indentLevel++;
				_script.MediumDisplayCasePrefabs[0] = (GameObject)EditorGUILayout.ObjectField("1x1", _script.MediumDisplayCasePrefabs[0], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[1] = (GameObject)EditorGUILayout.ObjectField("1x2", _script.MediumDisplayCasePrefabs[1], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[2] = (GameObject)EditorGUILayout.ObjectField("1x3", _script.MediumDisplayCasePrefabs[2], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[3] = (GameObject)EditorGUILayout.ObjectField("1x4", _script.MediumDisplayCasePrefabs[3], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[4] = (GameObject)EditorGUILayout.ObjectField("2x2", _script.MediumDisplayCasePrefabs[4], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[5] = (GameObject)EditorGUILayout.ObjectField("2x3", _script.MediumDisplayCasePrefabs[5], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[6] = (GameObject)EditorGUILayout.ObjectField("2x4", _script.MediumDisplayCasePrefabs[6], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[7] = (GameObject)EditorGUILayout.ObjectField("3x3", _script.MediumDisplayCasePrefabs[7], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[8] = (GameObject)EditorGUILayout.ObjectField("3x4", _script.MediumDisplayCasePrefabs[8], typeof(GameObject), false);
				_script.MediumDisplayCasePrefabs[9] = (GameObject)EditorGUILayout.ObjectField("4x4", _script.MediumDisplayCasePrefabs[9], typeof(GameObject), false);
				EditorGUI.indentLevel--;
			}
			_foldoutLowDisplayCaseList = EditorGUILayout.Foldout(_foldoutLowDisplayCaseList, "Low Prefabs");
			if(_foldoutLowDisplayCaseList){
				EditorGUI.indentLevel++;
				_script.LowDisplayCasePrefabs[0] = (GameObject)EditorGUILayout.ObjectField("1x1", _script.LowDisplayCasePrefabs[0], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[1] = (GameObject)EditorGUILayout.ObjectField("1x2", _script.LowDisplayCasePrefabs[1], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[2] = (GameObject)EditorGUILayout.ObjectField("1x3", _script.LowDisplayCasePrefabs[2], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[3] = (GameObject)EditorGUILayout.ObjectField("1x4", _script.LowDisplayCasePrefabs[3], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[4] = (GameObject)EditorGUILayout.ObjectField("2x2", _script.LowDisplayCasePrefabs[4], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[5] = (GameObject)EditorGUILayout.ObjectField("2x3", _script.LowDisplayCasePrefabs[5], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[6] = (GameObject)EditorGUILayout.ObjectField("2x4", _script.LowDisplayCasePrefabs[6], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[7] = (GameObject)EditorGUILayout.ObjectField("3x3", _script.LowDisplayCasePrefabs[7], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[8] = (GameObject)EditorGUILayout.ObjectField("3x4", _script.LowDisplayCasePrefabs[8], typeof(GameObject), false);
				_script.LowDisplayCasePrefabs[9] = (GameObject)EditorGUILayout.ObjectField("4x4", _script.LowDisplayCasePrefabs[9], typeof(GameObject), false);
				EditorGUI.indentLevel--;
			}

			foreach(GameObject obj in _script.HighDisplayCasePrefabs){
				if(obj == null){
					EditorGUILayout.HelpBox("Missing High Display Case Prefabs!", MessageType.Error);
					break;
				}
			}
			foreach(GameObject obj in _script.MediumDisplayCasePrefabs){
				if(obj == null){
					EditorGUILayout.HelpBox("Missing Medium Display Case Prefabs!", MessageType.Error);
					break;
				}
			}
			foreach(GameObject obj in _script.LowDisplayCasePrefabs){
				if(obj == null){
					EditorGUILayout.HelpBox("Missing Low Display Case Prefabs!", MessageType.Error);
					break;
				}
			}
			
			serializedObject.ApplyModifiedProperties();

			if(GUI.changed){
				EditorUtility.SetDirty(_script);
			}
		}
	}
}
