﻿using UnityEngine;
using UnityEditor;

namespace Viewport.MondoMuseum {

	[CustomEditor(typeof(WallFrameScriptableObject), true)]
	[CanEditMultipleObjects]
	public class WallFrameeScriptableObjectEditor : Editor{
		SerializedProperty _wallFrameCornerPrefab;
		SerializedProperty _wallFrameSidePrefab;
		SerializedProperty _wallFrameMaterialOptions;

		WallFrameScriptableObject _script;

		private void OnEnable(){
			_script = target as WallFrameScriptableObject;

			_wallFrameCornerPrefab = serializedObject.FindProperty("_wallFrameCornerPrefab");
			_wallFrameSidePrefab = serializedObject.FindProperty("_wallFrameSidePrefab");
		}

		public override void OnInspectorGUI(){
			serializedObject.Update();

			EditorGUILayout.PropertyField(_wallFrameCornerPrefab);
			EditorGUILayout.PropertyField(_wallFrameSidePrefab);
			
			serializedObject.ApplyModifiedProperties();

			if(GUI.changed){
				EditorUtility.SetDirty(_script);
			}
		}
	}
}
