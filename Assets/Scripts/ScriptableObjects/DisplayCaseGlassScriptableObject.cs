﻿using UnityEngine;

namespace Viewport.MondoMuseum {

	[CreateAssetMenu(fileName = "New Display Case Glass Data", menuName = "Mondo Museum Assets/Display Glass", order = 52)]
	[System.Serializable]
	public class DisplayCaseGlassScriptableObject : ScriptableObject {
		[SerializeField] private GameObject[] _displayCaseGlassPrefabs;
		public GameObject[] DisplayCaseGlassPrefabs{
			get { return _displayCaseGlassPrefabs; }
			set { _displayCaseGlassPrefabs = value; }
		}
	}
}