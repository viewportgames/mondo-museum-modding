﻿using UnityEngine;

namespace Viewport.MondoMuseum {

	[CreateAssetMenu(fileName = "New Wall Frame Data", menuName = "Mondo Museum Assets/Display Wall Frame", order = 53)]
	[System.Serializable]
	public class WallFrameScriptableObject : ScriptableObject {
		[SerializeField] private GameObject _wallFrameCornerPrefab;
		public GameObject WallFrameCornerPrefab{
			get { return _wallFrameCornerPrefab; }
			set { _wallFrameCornerPrefab = value; }
		}
		[SerializeField] private GameObject _wallFrameSidePrefab;
		public GameObject WallFrameSidePrefab{
			get { return _wallFrameSidePrefab; }
			set { _wallFrameSidePrefab = value; }
		}
	}
}