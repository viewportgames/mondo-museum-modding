﻿public enum ItemType{
	Path,
	WallMaterial,
	FloorMaterial,
	ExhibitItem,
	WallItem,
	Placeable
}

public enum ItemSubType{
	None,
	Door,
	Window,
	Carpet,
	VisitorRestItem,
	VisitorDrinkItem,
	VisitorFoodItem,
	VisitorToiletItem,
	WallFrame,
	Stanchion,
	ExhibitSign,
	Decorative,
	VisitorGiftShopItem,
	VisitorTicketPurchaseItem
}

public enum TileFootprint{
	_1x1 = 0,
	_1x2 = 1,
	_1x3 = 2,
	_1x4 = 3,
	_2x2 = 4,
	_2x3 = 5,
	_2x4 = 6,
	_3x3 = 7,
	_3x4 = 8,
	_4x4 = 9
}

public enum TileRestriction{
	Interior,
	Exterior,
	InteriorAndExterior
}
public enum WallRestriction{
	_Straight = 0,
	_2Corner = 1,
	_3Corner = 2,
	_4Corner = 3
}

public enum ExhibitItemRarity{
	Common = 0,
	Rare = 1,
	Unique = 2
}